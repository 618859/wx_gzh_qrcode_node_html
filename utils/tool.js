var { parseString } = require('xml2js')

module.exports = {
  getXMLStr(req) {
    return new Promise((resolve, reject) => {
      let data = '';
      // 以流的形式接收微信服务器发过来的数据
      req.on('data', (msg) => {
        // data 流是二进制的数据，要通过data.toString()转化为字符串
        data += msg.toString()
      })
      // 监听数据结束
      req.on('end', () => {
        resolve(data)
      })
    })
  },
  getJsData(data) {
    return new Promise((resolve, reject) => {
      parseString(data, (err,result) => {
        if (!err) {
          resolve(result)
        } else {
          reject('error')
        }
      })
    })
  },
  getObjData(obj){
    let tempObj={}
    if(obj && typeof obj === 'object'){
      // 循环对象，提取数据
      for(let key in obj){
        let value = obj[key]
        if(value && value.length > 0){
          tempObj[key] = value[0]
        }
      }
      return tempObj;
    } else{
      return tempObj;
    }
  }
}