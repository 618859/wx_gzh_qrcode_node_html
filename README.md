# wx_gzh_qrcode_node_html 微信公众号开发

> 1.该案例只是展示登录功能  
> 2.如果想获取用户的头像，昵称，openid等信息，需要在公众号里面打开该页面，然后拉起用户授权才能获取  
> 3.可参考这个案例 https://www.bilibili.com/video/BV13W4y1u7fy/?spm_id_from=333.999.0.0&vd_source=125d808bbbad2b8400f221b816a0f674  

   

#### 介绍
微信公众号：扫码登录  
有不懂的，可以加我微信号yizheng369  

### look效果

1.获取微信公众号二维码
![](./look/look1.jpg)

2.扫码登录
![](./look/look2.jpg)


---
## 以下为获取openid案例

### 视频教程
不懂的可以先看看这个视频：
https://www.bilibili.com/video/BV1XL411T73G/?vd_source=125d808bbbad2b8400f221b816a0f674

---
如何运行：
### 环境准备
你需要安装nodejs环境，
直接去nodejs官网https://nodejs.org/en/

### 下载项目源码到本地
https://gitee.com/618859/WeChat-official-account-openid.git


### 安装项目依赖
在本项目根目录运行以下命令  
一定要进入到：项目根目录，否则无法正确安装依赖
```
npm install
```

### 安装request模块
```
npm install request
```

### 启动后台:方法1
```
node app.js
```


### 启动后台:方法2：

可以安装了nodemon
```
npm i nodemon -g
```
### 或者如果你安装了nodemon的话
```
nodemon app.js
```

### 最后访问
在微信开发者工具中访问你内网穿透的地址
!['web2'](./look/web2.jpg)

